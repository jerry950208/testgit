package com.springInit.test.forRename;


import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.*;

/**
 * packageName    : com.springInit.test.forRename
 * fileName       : Json
 * crew           : 박영재 최예준 유택렬 양성훈
 * date           : 2022-07-13
 * description    :
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022-07-13        Sung-Hun Yang       최초 생성
 */
public class Json {
    public static void main(String[] args) throws IOException, NumberFormatException {
//        String viewDto_getMetaName = "org.strato.project";
//        String[] arr = viewDto_getMetaName.split("[.]");
//
//        String str1 = "src/main/java/com/springInit/test/main";
//        String str2 = "src/main/java/com/springInit/test/test_";
//        File mainDir;
//        File testDir;
//
//        for(int i=0; i<3; i++) {
//            str1 += "." + arr[i];
//            str2 += "." + arr[i];
//        }
//        mainDir = new File(str1);
//        mainDir.mkdirs();
//        testDir = new File(str2);
//        testDir.mkdirs();

//        for(int i=0; i<3; i++) {
//            str1 += arr[i];
//            str2 += arr[i];
//
//            mainDir = new File(str1);
//            testDir = new File(str2);
//            mainDir.mkdir();
//            testDir.mkdir();
//        }



        String readerPath = "C:\\Users\\Sung-Hun Yang\\Desktop\\신입사원_온보딩_프로젝트\\Test\\intellijj_test\\test\\src\\main\\java\\com\\springInit\\test\\Test.java";
        String writePath = "C:\\Users\\Sung-Hun Yang\\Desktop\\신입사원_온보딩_프로젝트\\Test\\intellijj_test\\test\\src\\main\\java\\com\\springInit\\test\\TestToTest.java";

        BufferedReader br_f = new BufferedReader(new FileReader(readerPath));
        BufferedWriter bw_f = new BufferedWriter(new FileWriter(writePath));

        String[] arr_line = null;
        String line = "";
        String beforePackage = "package com.springInit.test;";
        String afterPackage = "package com.springInit.test;";
        String beforeClassName = "public class Test {";
        String afterClassname = "public class TestToTest {";
         for(int i=1; (line = br_f.readLine()) != null; i++) {
             if(line.contains(beforePackage)){
                 line = afterPackage;
             } else if(line.equals(beforeClassName)) {
                 line = afterClassname;
             }

             bw_f.write(line + System.getProperty("line.separator"));   // '\n'과 같은 동작
         }

         br_f.close();
         bw_f.close();

    }
}
