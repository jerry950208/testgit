package com.springInit.test.Git;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.PushCommand;
import org.eclipse.jgit.api.RemoteAddCommand;
import org.eclipse.jgit.errors.RepositoryNotFoundException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.StoredConfig;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.URIish;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import java.io.File;
import java.util.Collection;

/**
 * packageName    : com.springInit.test
 * fileName       : JgitUtil
 * crew           : 박영재 최예준 유택렬 양성훈
 * date           : 2022-07-15
 * description    :
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022-07-15        Sung-Hun Yang       최초 생성
 */
public class JgitUtil {
    private static String userId = "jerry950208";
    private static String userPass = "12345678";
    private static String userName = "ysh0208";
    private static String userEmail = "ysh0208@strato.co.kr";
    private static String hash = "origin/test1";
    private static String url = "https://gitlab.com/jerry950208.git";
    private static CredentialsProvider cp = new UsernamePasswordCredentialsProvider(userId, userPass);

    public static Git init(File dir) throws Exception {
        return Git.init().setDirectory(dir).call();
        // .setInitialBranch("main")
    }

    public static  void remoteAdd(Git git) throws  Exception {
        // add remote repo;
        RemoteAddCommand remoteAddCommand = git.remoteAdd();
        remoteAddCommand.setName("origin");
        remoteAddCommand.setUri(new URIish(url));
        // 더 많은 세팅이 필요하면 여기다가 넣으면 된다

        remoteAddCommand.call();
    }

    public static void push(Git git) throws Exception {
        // Push to remote :
        PushCommand pushCommand = git.push();
        pushCommand.setCredentialsProvider(cp);
        // 세팅이 더 필요하면 여기에 추가
        pushCommand.call();
    }

    public static void add(Git git, String filePattern) throws  Exception {
        git.add().addFilepattern("").addFilepattern(filePattern).call();
    }

    public static void rm(Git git, String filePattern) throws Exception {
        git.rm().addFilepattern(filePattern).call();
    }

    public static void commit(Git git, String msg) throws Exception {
        // 커밋과 함께 메시지 등록
        git.commit()
                .setAuthor(userName, userEmail)
                .setMessage(msg)
                .call();
    }

    public static void pull(Git git) throws Exception {
        PullCommand pull = git.pull();
        pull.setCredentialsProvider(cp);
        pull.call();
    }

    public static void lsRemote(Git git) throws Exception {
        Collection<Ref> remoteRefs = git.lsRemote().setCredentialsProvider(cp).setRemote("origin").setTags(false).setHeads(true).call();
        for(Ref ref : remoteRefs) {
            System.out.println(ref.getName() + " -> " + ref.getObjectId().name());
        }
    }
    
    // 브렌치 변경
    public static void checkOut(File dir) throws  Exception {
        Git gitRepo = Git.cloneRepository().setURI(url) // Remote 주소
                .setDirectory(dir)                      // 다운받을 로컬의 위치
                .setNoCheckout(false)                    //
                .setCredentialsProvider(cp)             // 인증정보
                .call();

        gitRepo.checkout().setStartPoint(hash)      // origin / branch_name
                .addPath("dir")            // 다운받을 대상 경로
                .call();

        gitRepo.getRepository().close();
    }

    public static Git open(File dir) throws Exception {
        Git git = null;
        try {
            git = Git.open(dir);
        } catch (RepositoryNotFoundException e) {
            git = JgitUtil.init(dir);
        }
        return git;
    }
}
