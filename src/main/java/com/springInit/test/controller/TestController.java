package com.springInit.test.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * packageName    : com.springInit.test.controller
 * fileName       : TestController
 * crew           : 박영재 최예준 유택렬 양성훈
 * date           : 2022-07-08
 * description    :
 * ===========================================================
 * DATE              AUTHOR             NOTE
 * -----------------------------------------------------------
 * 2022-07-08        Sung-Hun Yang       최초 생성
 */
@Controller
public class TestController {

    @PostMapping("/testValue")
    public String springInitializrTest() {
        System.out.println("test");
        return "/:redirect";
    }
}
