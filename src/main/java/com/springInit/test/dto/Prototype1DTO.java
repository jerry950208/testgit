package com.springInit.test.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Prototype1DTO {
    private String project_build;       // Maven / Gradle
    private String language;            // Java / Kotlin / Groovy
    private String springBoot_ver;      // 3.0.0 (SNAPSHOT) / 3.0.0 (M3) / 2.7.2 (SNAPSHOT) / 2.7.1 / 2.6.10 (SNAPSHOT) / 2.6.9
    private String meta_group;          // com.example
    private String meta_artifact;       // demo
    private String meta_name;           // demo
    private String meta_desc;           // Demo project for Spring Boot
    private String meta_packageName;    // com.example.demo
    private String meta_packaging;      // Jar / War
    private String java_ver;            // 18 / 17 / 11 / 8
    private String oss_select;          // Redis / Kafka / MariaDB
    // 여기에 connection 값들을 받아올까?
    // 아니면 OSS마다 dto를 만들어서 추가를 할까 - 이렇게 할경우 매개변수를 OSS마다 추가해줘야 하는거 고려
}
